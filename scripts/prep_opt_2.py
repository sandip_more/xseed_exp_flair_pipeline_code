
import glob     
import pandas as pd
# from tqdm import tqdm
from difflib import SequenceMatcher
# import re
# import pickle
import random
import numpy as np
import pandas as pd
import glob 
role_company_date=None
import random
import pandas as pd
import re
from tqdm import tqdm
from difflib import SequenceMatcher
import pickle
import re
import datetime
from datetime import date
# import docx2txt
import glob
import os

import glob     
# import pandas as pd
# from tqdm import tqdm
from difflib import SequenceMatcher
# import re
# import pickle
import numpy as np
import pandas as pd
import glob    

def sublist_search(P, T):
    # P sublist to be searched for
    # T list to be searched in
    if len(P) == 0 or len(T) == 0 or len(T) < len(P):
        # print("cant process")
        return None
    # find all occurences of P[0] in T ....say L
    L = []
    for i in range(len(T)):
        # print(f"i:{i} T[i][0]:{T[i][0]}==P[0]:{P[0]}")
        if (T[i][0] == P[0]):
            L.append(i)
            # print(f"L:{L}")

    sp_result = None
    for pos in L:
        # print(f"pos:{pos}")
        tmp = [i[0] for i in T[pos:pos + len(P)]]
        if tmp == P:
            sp_result = pos
            break
    return sp_result


def matcher(string, pattern):
    '''
    Return the start and end index of any pattern present in the text.
    '''
    # print(f"STRING:{repr(string)} PATTERN:{repr(pattern)}")
    match_list = []
    pattern = pattern.strip()
    # print(f"PATTERN:{repr(pattern)}")
    seqMatch = SequenceMatcher(None, string, pattern, autojunk=False)
    match = seqMatch.find_longest_match(0, len(string), 0, len(pattern))
    # print(f"match.size:{match.size} == len(pattern):{len(pattern)}")
    if (match.size == len(pattern)):
        # print(f"match.a:{match.a}")
        start = match.a
        end = match.a + match.size
        match_tup = (start, end)
        string = string.replace(pattern, "X" * len(pattern), 1)
        match_list.append(match_tup)
        # print(match_list)

    return match_list, string


def mark_sentence(s, match_list):
    '''
    Marks all the entities in the sentence as per the BIO scheme.
    '''
    word_dict = []
    s_list = s.split()

    for word in s.split():
        word = [word, 'O']
        word_dict.append(word)
    # print(word_dict)

    for start, end, e_type in match_list:
        temp_str = s[start:end]
        tmp_list = temp_str.split()
        # print(f"tmp_list:{tmp_list}   word_dict:{word_dict}")
        sp = sublist_search(tmp_list, word_dict)
        # print(f"sp:{sp}")
        if (sp == None):
            continue
        ep = sp + len(tmp_list)

        if len(tmp_list) > 1:
            word_dict[sp][1] = 'B-' + e_type
            for i in range(sp + 1, ep):
                word_dict[i][1] = 'I-' + e_type
        else:
            word_dict[sp][1] = 'B-' + e_type

    return word_dict


def create_data(df, filepath):
    '''
    The function responsible for the creation of data in the said format.
    '''
    with open(filepath, 'w') as f:
        for text, annotation in zip(df.text, df.annotation):
            # print(f"text: {text} ")
            # print(f"annotation:{annotation} ")
            text_ = text
            match_list = []
            for i in annotation:
                # print(f"i:{i}  i 0:{i[0]} ")
                a, text_ = matcher(text, i[0])
                if (len(a) == 0):
                    continue
                # print(f'len(a):{len(a)}')
                # print(f'a[0][0]:{a[0][0]}')
                # print(f'a[0][1]:{a[0][1]}')
                # print(f'i[1]:{i[1]}')
                match_list.append((a[0][0], a[0][1], i[1]))
                # print(match_list)
            d = mark_sentence(text, match_list)
            for i in d:
                f.writelines(i[0] + ' ' + i[1] + '\n')
            f.writelines('\n')

def partition(start, end, array):
  # Initializing pivot's index to start
  pivot_index = start 
  pivot = array[pivot_index]
  t_pivot=list(pivot.keys())
  # print(f"t_pivot:{t_pivot[0]}")
    
  # This loop runs till start pointer crosses 
  # end pointer, and when it does we swap the
  # pivot with element on end pointer
  while start < end:
        
        # Increment the start pointer till it finds an 
        # element greater than  pivot
        # print(array[start])
        #{'ROLE': {'start_pos': 71, 'end_pos': 108}}
        # print(list(array[start].keys()))
        t_start=list(array[start].keys())
        # print(array[start][t_start[0]]['start_pos'])
        # print(pivot[t_pivot[0]]['start_pos'])
        while start < len(array) and array[start][t_start[0]]['start_pos'] <= pivot[t_pivot[0]]['start_pos']:
           start += 1
           try:
             t_start=list(array[start].keys())
           except IndexError:
              break
            
        # Decrement the end pointer till it finds an 
        # element less than pivot
        t_end=list(array[end].keys())
        while array[end][t_end[0]]['start_pos'] > pivot[t_pivot[0]]['start_pos']:
            end -= 1
            t_end=list(array[end].keys())
        
        # If start and end have not crossed each other, 
        # swap the numbers on start and end
        if(start < end):
            array[start], array[end] = array[end], array[start]
    
  # Swap pivot element with element on end pointer.
  # This puts pivot on its correct sorted place.
  array[end], array[pivot_index] = array[pivot_index], array[end]
   
  # Returning end pointer to divide the array into 2
  return end
    
# The main function that implements QuickSort 
def quick_sort(start, end, array):
   if (start < end):
        
        # p is partitioning index, array[p] 
        # is at right place
        p = partition(start, end, array)
        
        # Sort elements before partition 
        # and after partition
        quick_sort(start, p - 1, array)
        quick_sort(p + 1, end, array)



def getStartEndPosition(ent_list,txt):
  '''get start and end position
  '''
  result=[]
  for ent in ent_list:
   result.append(dict( {ent[1]: {'start_pos':txt.find(ent[0]),'end_pos':txt.find(ent[0])+len(ent[0])}}  ))
  return result
# getStartEndPosition(entity_list_0,text_0)

def getRecordFlairFormat(text_0,ent_list):
  L=[]
  E_L=[]
  global role_company_date
#   print("************************************************************************************")   
#   print(role_company_date)
#   print("*************************************************************************************")
  # print(len(ent_list))
  for i in range(len(ent_list)):
    if(i==0):
      if(ent_list[i][0]=="ROLE"):
        t1=text_0[0:ent_list[i][1]]
        t2=random.choice(role_company_date['roles'])
        L.append(t1+' '+t2)
        E_L.append((t2,"ROLE"))
      if(ent_list[i][0]=="COMPANY"):
        t1=text_0[0:ent_list[i][1]]
        t2=random.choice(role_company_date['companys'])
        L.append(t1+' '+t2)
        E_L.append((t2,"COMPANY"))
      if(ent_list[i][0]=="DATE"):
        t1=text_0[0:ent_list[i][1]]
        t2=random.choice(role_company_date['dates'])
        L.append(t1+' '+t2)
        E_L.append((t2,"DATE"))

    elif(i>0 and i<(len(ent_list))):
      
      if(ent_list[i][0]=="ROLE"):
        t1=(text_0[ent_list[i-1][2]:ent_list[i][1]])
        t2=random.choice(role_company_date['roles'])
        L.append(t1+' '+t2)
        E_L.append((t2,"ROLE"))
      if(ent_list[i][0]=="COMPANY"):
        t1=(text_0[ent_list[i-1][2]:ent_list[i][1]])
        t2=random.choice(role_company_date['companys'])
        L.append(t1+' '+t2)
        E_L.append((t2,"COMPANY"))
      if(ent_list[i][0]=="DATE"):
        t1=(text_0[ent_list[i-1][2]:ent_list[i][1]])
        t2=random.choice(role_company_date['dates'])
        L.append(t1+' '+t2)
        E_L.append((t2,"DATE"))

    else:
      pass
  L.append(text_0[ent_list[i][2]:])
  # print(len(L))
  text=" ".join(L)
  R=[text,E_L]
  return R

def generateDataforOneSample(s_e_0,text_0,rec_no,numberofrecords):
 import random
 data=[]
#  print(s_e_0)
 ent_list=[(list(ent.keys())[0],ent[list(ent.keys())[0]]['start_pos'],ent[list(ent.keys())[0]]['end_pos'])  for ent in s_e_0]
#  print((ent_list))
 for _ in range(numberofrecords):
   R=getRecordFlairFormat(text_0,ent_list)
  #  print(R)
   data.append(R)
 import pandas as pd
 import numpy as np
 df_Original = pd.DataFrame(data, columns=['text', 'annotation'])
  # df_synthetic=df_Original.sample(n=5)
 df_synthetic=df_Original
 train, dev, test = np.split(df_synthetic.sample(frac=1, random_state=42), [int(.6*len(df_synthetic)), int(.8*len(df_synthetic))])
 data_dict=dict({'train':train,'dev':dev,'test':test})
 import pickle
 with open('./assets/augmented_pkl/'+str(rec_no)+'.pkl', 'wb') as fh:
  pickle.dump(data_dict, fh)





def option_2():
    # 2. Read all pickle files from manual_pkl folder into data and create n number of augmented records from each record
  

    global role_company_date
    # Import pandas
    import pandas as pd
    ## Load the xlsx file
    excel_data = pd.read_excel('./assets/data/company.xlsx')

    # Read the values of the file in the dataframe
    data = pd.DataFrame(excel_data)
    ## Print the content
    # print(data)
    from pprint import pprint 
    companys = []
    for row in data.itertuples():
        b = (row[1].strip()+' '+row[2].strip())
        b = b.strip()
        companys.append(b)
    # pprint(companys)


    import pandas as pd

    # Load the xlsx file
    excel_data = pd.read_excel('./assets/data/role.xlsx')
    # Read the values of the file in the dataframe
    role_data = pd.DataFrame(excel_data)
    role = []
    for row in role_data.itertuples():
        u_l = (row[1])
        role.append(u_l)
        
    job_role=[]
    for i in role:
        # for ignore (text) 
        plain_edu = re.sub("[\(\[].*?[\)\]]", "", i)
        # \xa0 is unwanted space char. which is appears when we pest data
        plain_edu = plain_edu.replace('\xa0',' ')
        plain_edu = plain_edu.strip()
        plain_edu = plain_edu.replace("  "," ")
        job_role.append(plain_edu)
    job_role = list(set(job_role))








    date_list = ['Jan 1995 - january 1996', 'Jan 1995 TO january 1996', 'Jan 1995 to january 1996', 'AUG 1996 - JAN 1997', 'AUG 1996 TO JAN 1997', 'AUG 1996 to JAN 1997', 'JUL 1997 - nov 1998',
    'JUL 1997 TO nov 1998', 'JUL 1997 to nov 1998', 'SEPT 1998 - Nov 1999', 'SEPT 1998 TO Nov 1999', 'SEPT 1998 to Nov 1999', 'OCT 1999 - September 2000', 'OCT 1999 TO September 2000',
    'OCT 1999 to September 2000', 'NOV 2000 - aug 2001', 'NOV 2000 TO aug 2001', 'NOV 2000 to aug 2001', 'DEC 2001 - april 2002', 'DEC 2001 TO april 2002', 'DEC 2001 to april 2002',
    'JANUARY 2002 - SEPTEMBER 2003', 'JANUARY 2002 TO SEPTEMBER 2003', 'JANUARY 2002 to SEPTEMBER 2003', 'FEBRUARY 2003 - jan 2004', 'FEBRUARY 2003 TO jan 2004', 'FEBRUARY 2003 to jan 2004',
    'MARCH 2004 - FEBRUARY 2005', 'MARCH 2004 TO FEBRUARY 2005', 'MARCH 2004 to FEBRUARY 2005', 'APRIL 2005 - apr 2006', 'APRIL 2005 TO apr 2006', 'APRIL 2005 to apr 2006',
    'MAY 2006 - june 2007', 'MAY 2006 TO june 2007', 'MAY 2006 to june 2007', 'JUNE 2007 - November 2008', 'JUNE 2007 TO November 2008', 'JUNE 2007 to November 2008',
    'JULY 2008 - MAY 2009', 'JULY 2008 TO MAY 2009', 'JULY 2008 to MAY 2009', 'AUGUST 2009 - Nov 2010', 'AUGUST 2009 TO Nov 2010', 'AUGUST 2009 to Nov 2010',
    'SEPTEMBER 2010 - august 2011', 'SEPTEMBER 2010 TO august 2011', 'SEPTEMBER 2010 to august 2011', 'OCTOBER 2011 - october 2012', 'OCTOBER 2011 TO october 2012',
    'OCTOBER 2011 to october 2012', 'jan 2012 - Apr 2013', 'jan 2012 TO Apr 2013', 'jan 2012 to Apr 2013', 'feb 2013 - january 2014', 'feb 2013 TO january 2014',
    'feb 2013 to january 2014', 'mar 2014 - January 2015', 'mar 2014 TO January 2015', 'mar 2014 to January 2015', 'apr 2015 - september 2016', 'apr 2015 TO september 2016',
    'apr 2015 to september 2016', 'aug 2016 - february 2017', 'aug 2016 TO february 2017', 'aug 2016 to february 2017', 'Feb 2017 - MARCH 2018', 'Feb 2017 TO MARCH 2018',
    'Feb 2017 to MARCH 2018', 'Mar 2018 - November 2019', 'Mar 2018 TO November 2019', 'Mar 2018 to November 2019', 'Apr 2019 - JAN 2020', 'Apr 2019 TO JAN 2020', 'Apr 2019 to JAN 2020',
    'Aug 2020 - Mar 2021', 'Aug 2020 TO Mar 2021', 'Aug 2020 to Mar 2021', 'Jul 2021 - FEBRUARY 2022', 'Jul 2021 TO FEBRUARY 2022', 'Jul 2021 to FEBRUARY 2022',
    'Sept 2022 - AUG 2023', 'Sept 2022 TO AUG 2023', 'Sept 2022 to AUG 2023', 'Oct 1995 - JUNE 1996', 'Oct 1995 TO JUNE 1996', 'Oct 1995 to JUNE 1996', 'Oct 1996 - DEC 1997',
    'Oct 1996 TO DEC 1997', 'Oct 1996 to DEC 1997', 'Nov 1997 - may 1998', 'Nov 1997 TO may 1998', 'Nov 1997 to may 1998', 'Dec 1998 - Aug 1999', 'Dec 1998 TO Aug 1999',
    'Dec 1998 to Aug 1999', 'January 1999 - Jul 2000', 'January 1999 TO Jul 2000', 'January 1999 to Jul 2000', 'February 2000 - november 2001', 'February 2000 TO november 2001',
    'February 2000 to november 2001', 'March 2001 - july 2002', 'March 2001 TO july 2002', 'March 2001 to july 2002', 'April 2002 - May 2003', 'April 2002 TO May 2003', 'April 2002 to May 2003',
    'May 2003 - sept 2004', 'May 2003 TO sept 2004', 'May 2003 to sept 2004', 'November 2004 - AUGUST 2005', 'November 2004 TO AUGUST 2005', 'November 2004 to AUGUST 2005',
    'December 2005 - May 2006', 'December 2005 TO May 2006', 'December 2005 to May 2006', 'jul 2006 - june 2007', 'jul 2006 TO june 2007', 'jul 2006 to june 2007', 'sept 2007 - FEB 2008',
    'sept 2007 TO FEB 2008', 'sept 2007 to FEB 2008', 'oct 2008 - NOVEMBER 2009', 'oct 2008 TO NOVEMBER 2009', 'oct 2008 to NOVEMBER 2009', 'nov 2009 - oct 2010',
    'nov 2009 TO oct 2010', 'nov 2009 to oct 2010', 'dec 2010 - Dec 2011', 'dec 2010 TO Dec 2011', 'dec 2010 to Dec 2011', 'january 2011 - APRIL 2012', 'january 2011 TO APRIL 2012',
    'january 2011 to APRIL 2012', 'february 2012 - Jan 2013', 'february 2012 TO Jan 2013', 'february 2012 to Jan 2013', 'march 2013 - nov 2014', 'march 2013 TO nov 2014',
    'march 2013 to nov 2014', 'april 2014 - APR 2015', 'april 2014 TO APR 2015', 'april 2014 to APR 2015', 'jan 2015 - Mar 2016', 'jan 2015 TO Mar 2016', 'jan 2015 to Mar 2016',
    'feb 2016 - march 2017', 'feb 2016 TO march 2017', 'feb 2016 to march 2017', 'mar 2017 - july 2018', 'mar 2017 TO july 2018', 'mar 2017 to july 2018', 'apr 2018 - JUNE 2019',
    'apr 2018 TO JUNE 2019', 'apr 2018 to JUNE 2019', 'aug 2019 - September 2020', 'aug 2019 TO September 2020', 'aug 2019 to September 2020', 'jul 2020 - JUL 2021', 'jul 2020 TO JUL 2021',
    'jul 2020 to JUL 2021', 'sept 2021 - april 2022', 'sept 2021 TO april 2022', 'sept 2021 to april 2022', 'oct 2022 - APR 2023', 'oct 2022 TO APR 2023', 'oct 2022 to APR 2023',
    'nov 1995 - June 1996', 'nov 1995 TO June 1996', 'nov 1995 to June 1996', 'dec 1996 - APRIL 1997', 'dec 1996 TO APRIL 1997', 'dec 1996 to APRIL 1997', 'january 1997 - JULY 1998',
    'january 1997 TO JULY 1998', 'january 1997 to JULY 1998', 'february 1998 - NOVEMBER 1999', 'february 1998 TO NOVEMBER 1999', 'february 1998 to NOVEMBER 1999', 'march 1999 - Apr 2000',
    'march 1999 TO Apr 2000', 'march 1999 to Apr 2000', 'april 2000 - OCTOBER 2001', 'april 2000 TO OCTOBER 2001', 'april 2000 to OCTOBER 2001', 'may 2001 - feb 2002', 'may 2001 TO feb 2002',
    'may 2001 to feb 2002', 'may 2002 - mar 2003', 'may 2002 TO mar 2003', 'may 2002 to mar 2003', 'june 2003 - Oct 2004', 'june 2003 TO Oct 2004', 'june 2003 to Oct 2004',
    'july 2004 - JANUARY 2005', 'july 2004 TO JANUARY 2005', 'july 2004 to JANUARY 2005', 'august 2005 - OCTOBER 2006', 'august 2005 TO OCTOBER 2006', 'august 2005 to OCTOBER 2006',
    'september 2006 - SEPT 2007', 'september 2006 TO SEPT 2007', 'september 2006 to SEPT 2007', 'october 2007 - August 2008', 'october 2007 TO August 2008', 'october 2007 to August 2008',
    'november 2008 - August 2009', 'november 2008 TO August 2009', 'november 2008 to August 2009', 'december 2009 - JANUARY 2010', 'december 2009 TO JANUARY 2010',
    'december 2009 to JANUARY 2010', 'JAN 2010 - Oct 2011', 'JAN 2010 TO Oct 2011', 'JAN 2010 to Oct 2011', 'FEB 2011 - Feb 2012', 'FEB 2011 TO Feb 2012', 'FEB 2011 to Feb 2012',
    'MAR 2012 - OCT 2013', 'MAR 2012 TO OCT 2013', 'MAR 2012 to OCT 2013', 'July 2013 - Aug 2014', 'July 2013 TO Aug 2014', 'July 2013 to Aug 2014', 'August 2014 - April 2015',
    'August 2014 TO April 2015', 'August 2014 to April 2015', 'September 2015 - Dec 2016', 'September 2015 TO Dec 2016', 'September 2015 to Dec 2016', 'October 2016 - jul 2017',
    'October 2016 TO jul 2017', 'October 2016 to jul 2017', 'November 2017 - jul 2018', 'November 2017 TO jul 2018', 'November 2017 to jul 2018', 'Nov 2018 - AUG 2019', 'Nov 2018 TO AUG 2019',
    'Nov 2018 to AUG 2019', 'Dec 2019 - MAR 2020', 'Dec 2019 TO MAR 2020', 'Dec 2019 to MAR 2020', 'January 2020 - Sept 2021', 'January 2020 TO Sept 2021', 'January 2020 to Sept 2021',
    'February 2021 - July 2022', 'February 2021 TO July 2022', 'February 2021 to July 2022', 'APR 2022 - DECEMBER 2023', 'APR 2022 TO DECEMBER 2023', 'APR 2022 to DECEMBER 2023',
    'Jan 1995 - AUGUST 1996', 'Jan 1995 TO AUGUST 1996', 'Jan 1995 to AUGUST 1996', 'Feb 1996 - december 1997', 'Feb 1996 TO december 1997', 'Feb 1996 to december 1997',
    'Mar 1997 - oct 1998', 'Mar 1997 TO oct 1998', 'Mar 1997 to oct 1998', 'JANUARY 1998 - Jul 1999', 'JANUARY 1998 TO Jul 1999', 'JANUARY 1998 to Jul 1999', 'FEBRUARY 1999 - feb 2000',
    'FEBRUARY 1999 TO feb 2000', 'FEBRUARY 1999 to feb 2000', 'MARCH 2000 - SEPT 2001', 'MARCH 2000 TO SEPT 2001', 'MARCH 2000 to SEPT 2001', 'june 2001 - august 2002',
    'june 2001 TO august 2002', 'june 2001 to august 2002', 'july 2002 - December 2003', 'july 2002 TO December 2003', 'july 2002 to December 2003', 'august 2003 - FEB 2004',
    'august 2003 TO FEB 2004', 'august 2003 to FEB 2004', 'FEB 2004 - march 2005', 'FEB 2004 TO march 2005', 'FEB 2004 to march 2005', 'MAR 2005 - DECEMBER 2006',
    'MAR 2005 TO DECEMBER 2006', 'MAR 2005 to DECEMBER 2006', 'APR 2006 - NOV 2007', 'APR 2006 TO NOV 2007', 'APR 2006 to NOV 2007', 'NOVEMBER 2007 - April 2008', 'NOVEMBER 2007 TO April 2008',
    'NOVEMBER 2007 to April 2008', 'DECEMBER 2008 - may 2009', 'DECEMBER 2008 TO may 2009', 'DECEMBER 2008 to may 2009', 'september 2009 - DEC 2010', 'september 2009 TO DEC 2010',
    'september 2009 to DEC 2010', 'october 2010 - February 2011', 'october 2010 TO February 2011', 'october 2010 to February 2011', 'november 2011 - dec 2012', 'november 2011 TO dec 2012',
    'november 2011 to dec 2012', 'december 2012 - mar 2013', 'december 2012 TO mar 2013', 'december 2012 to mar 2013', 'JAN 2013 - SEPTEMBER 2014',
    'JAN 2013 TO SEPTEMBER 2014', 'JAN 2013 to SEPTEMBER 2014', 'OCT 2014 - January 2015', 'OCT 2014 TO January 2015', 'OCT 2014 to January 2015',
    'NOV 2015 - October 2016', 'NOV 2015 TO October 2016', 'NOV 2015 to October 2016', 'DEC 2016 - MAY 2017', 'DEC 2016 TO MAY 2017', 'DEC 2016 to MAY 2017',
    'June 2017 - aug 2018', 'June 2017 TO aug 2018', 'June 2017 to aug 2018', 'March 2018 - Sept 2019', 'March 2018 TO Sept 2019', 'March 2018 to Sept 2019',
    'April 2019 - March 2020', 'April 2019 TO March 2020', 'April 2019 to March 2020', 'May 2020 - dec 2021', 'May 2020 TO dec 2021', 'May 2020 to dec 2021',
    'June 2021 - december 2022', 'June 2021 TO december 2022', 'June 2021 to december 2022', 'July 2022 - March 2023', 'July 2022 TO March 2023', 'July 2022 to March 2023']


    dates=date_list
    role_company_date=dict({'companys':companys,'roles':job_role,'dates':dates})
    # print(role_company_date)
    import pickle
    with open('./assets/data/role_company_date.pkl', 'wb') as fh:
        pickle.dump(role_company_date, fh)








    import pickle
    
    with open ('./assets/data/role_company_date.pkl', "rb") as pickle_off:
        role_company_date = pickle.load(pickle_off)

    # get data labelled using GUI from flairDataset folder in FLAIR format into records
    import pickle
    import glob 
    pkl_files=(glob.glob(r"./assets/manual_pkl/*.pkl"))
    records=[]
    for file in pkl_files:
        with open (file, "rb") as pickle_off:
            d= pickle.load(pickle_off)
            records.extend(d)
    
   #remove location
   
    for r in records:  
        r_0=r#first record
        text_0=r_0[0]
        entity_list_0=r_0[1]
        f=False
        loc_ind=None
        e=None
        for e in entity_list_0:
            if(e[1]=="LOCATION"):
                f=True
                loc_ind=entity_list_0.index(e)
                print(entity_list_0.index(e))
                break
        if(f):
            # print(r[1])
            entity_list_0.remove(e)
            # print(r[1])
   


    count=0
    for r in records:    
        r_0=r#first record
        text_0=r_0[0]
        entity_list_0=r_0[1]
        # print(text_0)
        # print(entity_list_0)
        
        # # Python program for implementation of Selection# Sort
        import sys
        s_e_0=getStartEndPosition(entity_list_0,text_0)
        # print(f"start end: {s_e_0}")
        if(len(s_e_0)>0):
            array = s_e_0
            #  print(f'record index:{records.index(r)}')
            #  print(array)
            quick_sort(0, len(array) - 1, array)
            #  print(array)
            numberofrecords=100    
            generateDataforOneSample(array,text_0,records.index(r),numberofrecords)
            count=count+1
        


  

    import glob 
    import pandas as pd
    pkl_files=(glob.glob(r"./assets/augmented_pkl/*.pkl"))
    print(pkl_files)
    import pickle

    dev_t=[]
    dev_a=[]
    test_t=[]
    test_a=[]
    train_t=[]
    train_a=[]
    # print(dev)
    for file in pkl_files:
        with open (file, "rb") as pickle_off:
            data= pickle.load(pickle_off)
            dev_t.extend(list(data['dev']['text']))
            dev_a.extend(list(data['dev']['annotation']))
            test_t.extend(list(data['test']['text']))
            test_a.extend(list(data['test']['annotation']))
            train_t.extend(list(data['train']['text']))
            train_a.extend(list(data['train']['annotation']))

        # dev_t.append(list(data['dev']['text']))   
    # print(dev_t)
    # print(dev_a)

    dev=pd.DataFrame(dict({'text':dev_t,'annotation':dev_a}))
    test=pd.DataFrame(dict({'text':test_t,'annotation':test_a}))
    train=pd.DataFrame(dict({'text':train_t,'annotation':train_a}))


    #***************************************************************************************************************************************
    #uncomment below line for obtaining lesser data set
    
    dev=dev[0:5]                         
    test=test[0:5]
    train=train[0:10]
    #***************************************************************************************************************************************





    
    ## path to save the train txt file.
    filepath_train = r'.\corpus\train.txt'
    ## path to save the test txt file.
    filepath_test =  r'.\corpus\test.txt'
    ## path to save the validation txt file.
    filepath_dev =  r'.\corpus\dev.txt'
    # file_test_2106='/content/drive/My Drive/FLAIR_EXP_NER/dev_2106.txt'

    # # ## creating the file.
    create_data(train, filepath_train)
    create_data(test, filepath_test)
    create_data(dev, filepath_dev)

