# ======================== Flair training ========================

# define columns
import encodings
from flair.datasets import ColumnCorpus
from flair.data import Corpus
import mlflow
from sklearn.model_selection import ParameterGrid
import os

columns = {0 : 'text', 1 : 'ner'}
# directory where the data resides
data_folder = '.\corpus'
# initializing the corpus
corpus: Corpus = ColumnCorpus(data_folder, columns, train_file = 'train.txt',test_file = 'test.txt',dev_file = 'dev.txt',encoding='cp1252')
print(len(corpus.dev))
print(len(corpus.test))
print(len(corpus.train))


# tag to predict
tag_type = 'ner'
# make tag dictionary from the corpus
tag_dictionary = corpus.make_tag_dictionary(tag_type=tag_type)
print(tag_dictionary)
from flair.embeddings import WordEmbeddings, FlairEmbeddings

# init standard GloVe embedding
glove_embedding = WordEmbeddings('glove')

# init Flair forward and backwards embeddings
flair_embedding_forward = FlairEmbeddings('news-forward')
flair_embedding_backward = FlairEmbeddings('news-backward')


# Now instantiate the StackedEmbeddings class and pass it a list containing these two embeddings.
from flair.embeddings import StackedEmbeddings

# create a StackedEmbedding object that combines glove and forward/backward flair embeddings
stacked_embeddings = StackedEmbeddings([
                                        glove_embedding,
                                        flair_embedding_forward,
                                        flair_embedding_backward,
                                       ])






# hyperparameter tuning training 
# Pickle file read
import pickle
pickle_out = open(".\Exp.pickle", "rb")
c = pickle.load(pickle_out)
pickle_out.close()
experiment_name = "Experiment_" + str(c)
c = c + 1
pickle_out = open(".\Exp.pickle", "wb")
pickle.dump(c, pickle_out)
pickle_out.close()

# Experiment for MLFlow
# os.environ['MLFLOW_TRACKING_URI'] = 'postgresql://mlflow:mlflow@localhost/xseed_exp_db'
# mlflow.set_tracking_uri('postgresql://mlflow:mlflow@localhost/xseed_exp_db')
experiment_id = mlflow.create_experiment(experiment_name)
# mlflow.set_experiment(experiment_name)
print(experiment_id)
experiment = mlflow.get_experiment(experiment_id)
print("Name: {}".format(experiment.name))
print("Experiment_id: {}".format(experiment.experiment_id))
print("Artifact Location: {}".format(experiment.artifact_location))
print("Tags: {}".format(experiment.tags))
print("Lifecycle_stage: {}".format(experiment.lifecycle_stage))

# param_grid = {'learning_rate':[0.001,0.01,0.1],
#               'mini_batch_size':[32,64,128],
#               'max_epochs':[30,60],'train_with_dev':[False,True]}
param_grid = {'learning_rate':[0.1],
              'mini_batch_size':[32],
              'max_epochs':[1,2],'train_with_dev':[False]}
parameters = list(ParameterGrid(param_grid))





# define columns
from flair.datasets import ColumnCorpus
from flair.data import Corpus

columns = {0 : 'text', 1 : 'ner'}
# directory where the data resides
data_folder = './corpus/'
# initializing the corpus
corpus: Corpus = ColumnCorpus(data_folder, columns, train_file = 'train.txt',test_file = 'test.txt',dev_file = 'dev.txt',encoding='cp1252')
print(len(corpus.dev))
print(len(corpus.test))
print(len(corpus.train))



# tag to predict
tag_type = 'ner'
# make tag dictionary from the corpus
tag_dictionary = corpus.make_tag_dictionary(tag_type=tag_type)
print(tag_dictionary)
from flair.embeddings import WordEmbeddings, FlairEmbeddings

# init standard GloVe embedding
glove_embedding = WordEmbeddings('glove')

# init Flair forward and backwards embeddings
flair_embedding_forward = FlairEmbeddings('news-forward')
flair_embedding_backward = FlairEmbeddings('news-backward')


# Now instantiate the StackedEmbeddings class and pass it a list containing these two embeddings.
from flair.embeddings import StackedEmbeddings

# create a StackedEmbedding object that combines glove and forward/backward flair embeddings
stacked_embeddings = StackedEmbeddings([
                                        glove_embedding,
                                        flair_embedding_forward,
                                        flair_embedding_backward,
                                       ])


from flair.models import SequenceTagger
from flair.data import Dictionary
# tag_ditionary = Dictionary()
tagger : SequenceTagger = SequenceTagger(hidden_size=256, embeddings=stacked_embeddings, tag_dictionary=tag_dictionary,tag_type=tag_type,use_crf=True)
print(tagger)


from flair.trainers import ModelTrainer
trainer : ModelTrainer = ModelTrainer(tagger, corpus)





count = 0
for params in parameters:
    with mlflow.start_run(experiment_id=experiment_id) as run:
        # print(params)
        count += 1
        print(f"Count is: {count}")
        print(params)
        
        directory = str(count)
        # Parent Directory path 
        parent_dir = r".\training"
        # Path 
        path = os.path.join(parent_dir, directory)     
        # Create the directory 
        try:
            os.mkdir(path) 
        except FileExistsError:
            pass
        history=trainer.train(path,
              learning_rate=params['learning_rate'],
              mini_batch_size=params['mini_batch_size'],
              max_epochs=params['max_epochs'],
              train_with_dev=params['train_with_dev'])
        print(history)
        
        mlflow.log_param('learning_rate',params['learning_rate'])
        mlflow.log_param('mini_batch_size',params['mini_batch_size'])
        mlflow.log_param('max_epochs',params['max_epochs'])
        mlflow.log_param('max_epochs',params['max_epochs'])
        mlflow.log_param('train_with_dev',params['train_with_dev'])
        
        mlflow.log_metric('test_score',history['test_score'])
        
        mlflow.log_artifacts(path)
        # mlflow.log_param("data_company_url", cmpny_url)
        # mlflow.log_param("data_job_role_url", job_url)
        # mlflow.log_param("data_version", version)

