import glob     
# import pandas as pd
# from tqdm import tqdm
from difflib import SequenceMatcher
# import re
# import pickle
import numpy as np
import pandas as pd
import glob    

def sublist_search(P, T):
    # P sublist to be searched for
    # T list to be searched in
    if len(P) == 0 or len(T) == 0 or len(T) < len(P):
        # print("cant process")
        return None
    # find all occurences of P[0] in T ....say L
    L = []
    for i in range(len(T)):
        # print(f"i:{i} T[i][0]:{T[i][0]}==P[0]:{P[0]}")
        if (T[i][0] == P[0]):
            L.append(i)
            # print(f"L:{L}")

    sp_result = None
    for pos in L:
        # print(f"pos:{pos}")
        tmp = [i[0] for i in T[pos:pos + len(P)]]
        if tmp == P:
            sp_result = pos
            break
    return sp_result


def matcher(string, pattern):
    '''
    Return the start and end index of any pattern present in the text.
    '''
    # print(f"STRING:{repr(string)} PATTERN:{repr(pattern)}")
    match_list = []
    pattern = pattern.strip()
    # print(f"PATTERN:{repr(pattern)}")
    seqMatch = SequenceMatcher(None, string, pattern, autojunk=False)
    match = seqMatch.find_longest_match(0, len(string), 0, len(pattern))
    # print(f"match.size:{match.size} == len(pattern):{len(pattern)}")
    if (match.size == len(pattern)):
        # print(f"match.a:{match.a}")
        start = match.a
        end = match.a + match.size
        match_tup = (start, end)
        string = string.replace(pattern, "X" * len(pattern), 1)
        match_list.append(match_tup)
        # print(match_list)

    return match_list, string


def mark_sentence(s, match_list):
    '''
    Marks all the entities in the sentence as per the BIO scheme.
    '''
    word_dict = []
    s_list = s.split()

    for word in s.split():
        word = [word, 'O']
        word_dict.append(word)
    # print(word_dict)

    for start, end, e_type in match_list:
        temp_str = s[start:end]
        tmp_list = temp_str.split()
        # print(f"tmp_list:{tmp_list}   word_dict:{word_dict}")
        sp = sublist_search(tmp_list, word_dict)
        # print(f"sp:{sp}")
        if (sp == None):
            continue
        ep = sp + len(tmp_list)

        if len(tmp_list) > 1:
            word_dict[sp][1] = 'B-' + e_type
            for i in range(sp + 1, ep):
                word_dict[i][1] = 'I-' + e_type
        else:
            word_dict[sp][1] = 'B-' + e_type

    return word_dict


def create_data(df, filepath):
    '''
    The function responsible for the creation of data in the said format.
    '''
    with open(filepath, 'w') as f:
        for text, annotation in zip(df.text, df.annotation):
            # print(f"text: {text} ")
            # print(f"annotation:{annotation} ")
            text_ = text
            match_list = []
            for i in annotation:
                # print(f"i:{i}  i 0:{i[0]} ")
                a, text_ = matcher(text, i[0])
                if (len(a) == 0):
                    continue
                # print(f'len(a):{len(a)}')
                # print(f'a[0][0]:{a[0][0]}')
                # print(f'a[0][1]:{a[0][1]}')
                # print(f'i[1]:{i[1]}')
                match_list.append((a[0][0], a[0][1], i[1]))
                # print(match_list)
            d = mark_sentence(text, match_list)
            for i in d:
                f.writelines(i[0] + ' ' + i[1] + '\n')
            f.writelines('\n')


def option_1():
    # 1. Read all pickle files from manual_pkl folder and store them
    # 2.
    

    pkl_files = (glob.glob(r".\assets\manual_pkl\*.pkl"))
    print(pkl_files)
    import pickle

    data = []
    # print(dev)
    for file in pkl_files:
        with open(file, "rb") as pickle_off:
            d = pickle.load(pickle_off)
            data.extend(d)
            # print(len(data))
            # print(type(data))
            # print(data[0])

    
   #remove location
    records=data
    for r in records:  
        r_0=r#first record
        text_0=r_0[0]
        entity_list_0=r_0[1]
        f=False
        loc_ind=None
        e=None
        for e in entity_list_0:
            if(e[1]=="LOCATION"):
                f=True
                loc_ind=entity_list_0.index(e)
                print(entity_list_0.index(e))
                break
        if(f):
            # print(r[1])
            entity_list_0.remove(e)
            # print(r[1])



    #***************************************************************************************************************************************
    #uncomment below line for obtaining lesser data set
    data=data[0:100]                          
    #***************************************************************************************************************************************
    
    
    
    
    
    df_Original = pd.DataFrame(data, columns=['text', 'annotation'])
    df_synthetic = df_Original
    train, dev, test = np.split(df_synthetic.sample(frac=1, random_state=42),
                                [int(.6 * len(df_synthetic)), int(.8 * len(df_synthetic))])
    ## path to save the train txt file.
    filepath_train = r'.\corpus\train.txt'
    ## path to save the test txt file.
    filepath_test =  r'.\corpus\test.txt'
    ## path to save the validation txt file.
    filepath_dev =  r'.\corpus\dev.txt'
    # file_test_2106='/content/drive/My Drive/FLAIR_EXP_NER/dev_2106.txt'

    # # ## creating the file.
    create_data(train, filepath_train)
    create_data(test, filepath_test)
    create_data(dev, filepath_dev)
